#!/usr/bin/env python3

"""
class to handle UC Library Website

kenzie tandun
2019-02-27
"""

from bs4 import BeautifulSoup as bs
import requests
from termcolor import cprint
import time
import database
import os
from config import uclib_default_header
from config import uclib_proxy_header

class UCLibrary():
    def __init__(self, username, password, courses):
        """constructor for UCLibrary object"""
        self.username = username
        self.password = password
        self.session = requests.Session()

        self.default_header = uclib_default_header
        self.session.headers.update(self.default_header)

    def parse_past_papers(self, course):
        """given a course, parse its course webpage and find past papers
        PDF
        
        Returns: List of Past Papers PDF urls"""
        cprint("[INFO] Finding past papers for {}".format(course), 
                "blue")
        course_page = "http://library.canterbury.ac.nz/exams/index.php?course={course}&year=".format(course=course)
        resp = self.session.get(course_page)
        soup = bs(resp.text, "html.parser")

        main_section = soup.find("main", 
                {"id": "main", "class": "col-sm-8 col-md-9 main content-col"})

        pdf_urls = []
        #pdf_url_template = "http://library.canterbury.ac.nz.ezproxy.canterbury.ac.nz/"
        for link in main_section.find_all("div", {"class": "col-lg-12"}):
            for pdf_link in link.find_all("a", href=True):
                pdf_url = pdf_link.get("href")
                pdf_urls.append(pdf_url)
                #pdf_url = '/'.join(pdf_url.split('/')[6:])
                #pdf_urls.append(pdf_url_template + pdf_url)

        return pdf_urls

    def login(self):
        """login to UCLibrary website as your account in UC"""
        cprint("[INFO] Logging in", "green")
        payload = {"user": self.username,
                   "pass": self.password}


        url = "https://login.ezproxy.canterbury.ac.nz/login"
        resp = self.session.post(url, data=payload)

        ezproxy_cookie = self.session.cookies.items()[0]
        _, cookie = ezproxy_cookie

        self.session.headers.update({"Cookie": "ezproxy={}".format(cookie)})

        if "That username or password was incorrect" in resp.text:
            cprint("[ERR] Unable to log in", "red")
            cprint("[ERR] Wrong username/password?", "red")
            sys.exit(1)

        cprint("[INFO] Logged in", "green")

    def download_pdf(self, c, pdf_url, location):
        """downloads pdf from <<pdf_url>>
        updates the headers so the requests come through the login page
        then updates the headers again to access the file
        """
        self.session.headers.update(self.default_header)
        r = self.session.get(pdf_url, allow_redirects=True)
        soup = bs(r.text, "html.parser")
        link = ""
        for i in soup.find_all('a', href=True):
            link = i.get("href")

        self.session.headers.update(uclib_proxy_header)

        target_dir = location + '/' 
        database.download_pdf(self.session, c, link, target_dir)
