#!/usr/bin/env python3

from slackclient import SlackClient
from uclibrary import UCLibrary
import configparser
import learn
import os
import database

HERE = os.path.dirname(os.path.abspath(__file__)) + '/'

def send_message(slack_token, message):
    """sends message to slack using slack api"""
    sc = SlackClient(slack_token)
    sc.api_call(
        "chat.postMessage",
        channel = "#general",
        username = "awesome-bot",
        icon_emoji = ":robot_face:",
        text = message
        )

def main():
    """main program"""
    learn.check_config_file("config.ini")
    cfg = configparser.ConfigParser()
    cfg.read(HERE + "config.ini")
    conn, c = database.init_db()
    username = cfg.get("login", "username")
    password = cfg.get("login", "password")
    courses = cfg.get("courses", "courses")
    slack_token = cfg.get("slack", "token")

    download_directory = cfg.get("download",
            "download_directory")
    lrn = learn.Learn(conn, c,
            username,
            password,
            courses,
            slack_token=slack_token)
    resp = lrn.login()
    courses_dict = lrn.get_courses(resp)
    for course_id, course_subject in courses_dict.items():
        course_notes = lrn.get_course_notes(course_id, course_subject)
        for course_section, course_pdfs in course_notes.items():
            for course_pdf in course_pdfs:
                database.download_pdf(lrn.session, lrn.c, 
                        course_pdf, "{}/{}/{}".format(
                            download_directory,
                            course_subject,
                            course_section))
                database.commit(conn)

    print("Processing Past Papers:")
    uclib = UCLibrary(username, password, courses)
    uclib.login()
    for course in courses.split(','):
        pdfs = uclib.parse_past_papers(course)
        for pdf in pdfs:
            uclib.download_pdf(lrn.c, pdf, 
                    "{}/Past Papers/{}".format(
                        download_directory, course))

    if slack_token and lrn.slack_tmpl:
        downloaded_files = "\n".join(lrn.slack_tmpl)
        message = "Downloaded:\n"
        message += downloaded_files
        send_message(slack_token, message)

    print("Done.")
    if lrn.written_log:
        print("There were some errors written to logfile:")
        print("pylearn.log")

if __name__ == "__main__":
    main()
