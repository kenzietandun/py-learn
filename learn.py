#!/usr/bin/env python3

"""
a small script to download all pdf files from
LEARN website

might be not pylint compliant ;)

kenzie tandun
2018-03-05
"""

from bs4 import BeautifulSoup as bs
from termcolor import cprint
import requests
import sqlite3
import time
import sys
import os
import re
import database
from config import HERE
from config import LOG

if not os.path.isfile(LOG):
    os.mknod(LOG)

def logger(f):
    """logger decorator
    to use it, add @logger on TOP of the function
    you need to log
    e.g.

    @logger
    def init_db():
        blah...
    """

    def new_f(*args):
        start = time.time()
        cprint("***LOG {}({})".format(f.__name__,
            args), "magenta")
        end = time.time()
        cprint("***ELAPSED: {}s".format(end - start),
                "magenta")
        res = f(*args)

    return new_f

class Learn:
    def __init__(self, conn, c, username, password, courses, slack_token=""):
        """constructor for learn object"""
        self.username = username
        self.password = password
        self.session = requests.Session()
        courses = courses.upper()
        self.subscribed_courses = tuple(courses.split(','))
        print("Searching for {}".format(self.subscribed_courses))
        self.conn, self.c = conn, c
        self.written_log = False
        self.slack_token = slack_token
        if self.slack_token:
            self.slack_tmpl = []

    def login(self):
        """login as your account in UC
        Returns main page html text in String"""
        cprint("[INFO] Logging in", "green")
        payload = {"username": self.username,
                   "password": self.password}

        resp = self.session.post("https://learn.canterbury.ac.nz/login/index.php",
                data=payload)

        if "You are not logged in." in resp.text:
            cprint("[ERR] Unable to log in", "red")
            cprint("[ERR] Wrong username/password?", "red")
            sys.exit(1)

        cprint("[INFO] Logged in", "green")
        time.sleep(1)
        return resp.text

    def get_courses(self, main_page_html):
        """get list of courses id
        Returns dict of course_id --> course_name"""
        soup = bs(main_page_html, "html.parser")
        courses = {}
        for link in soup.find_all("a", href=True):
            courses_links = link.get("href")
            course_name = link.text
            if courses_links.startswith("https://learn.canterbury.ac.nz/course/view.php?id="):
                matches = re.match(r".*canterbury\.ac\.nz\/course\/view\.php\?id=(\d+)$",
                                courses_links)
                if matches and course_name.startswith(self.subscribed_courses):
                    courses[matches[1]] = course_name

        return courses

    def get_course_notes(self, course_id, course_name):
        """get list of notes url for course <<course_id>>

        able to parse normal links and folder links

        Returns dict of course's section --> list of PDFs"""
        course_url = "http://learn.canterbury.ac.nz/course/view.php?id={id}"
        course_url = course_url.format(id=course_id)
        cprint("[INFO] Processing {name}".format(name=course_name), "blue")
        resp = self.session.get(course_url)
        course_notes = {}
        sections = []
        soup = bs(resp.text, "html.parser")

        course_pdfs = []
        for main_page in soup.find_all("ul", {"class": "section img-text"}):
            for link in main_page.find_all('a', href=True):
                pdf_url = link.get("href")
                if "mod/resource" in pdf_url and \
                    not database.is_downloaded(pdf_url, self.conn, self.c):
                    cprint("   +   Adding {} to queue".format(pdf_url), "green")
                    course_pdfs.append(pdf_url)

        course_notes["Main page"] = course_pdfs

        for section in soup.find_all("ul", {"class": "list section-list"}):
            for link in section.find_all("a", href=True):
                matches = re.match(r".*=(\d+)$", link.get("href"))
                section_name = link.text.upper()
                if matches:
                    sections.append((matches[1], section_name))

        for section_id, section_name in sections:
            course_pdfs = []
            cprint("   +   Processing {name}".format(name=section_name),
                    "red", end='\r')
            course_section_url = "{root}&section={i}".format(
                    root=course_url, i=section_id)
            resp = self.session.get(course_section_url)
            soup = bs(resp.text, "html.parser")
            course_contents = soup.find("div", {"class": "course-content"})
            for link in course_contents.find_all('a', href=True):
                pdf_url = link.get("href")
                if "mod/resource" in pdf_url and \
                    not database.is_downloaded(pdf_url, self.conn, self.c):
                    cprint("   +   Adding {} to queue".format(pdf_url), "green")
                    course_pdfs.append(pdf_url)
                if "mod/folder" in pdf_url:
                    files = self.process_course_folder(pdf_url)
                    course_pdfs.extend(files)

            course_notes[section_name] = course_pdfs
            cprint("   ✓   Processing {name}".format(name=section_name),
                    "blue")

        return course_notes

    def process_course_folder(self, folder_url):
        """process a folder from course,
        checks if folder contains downloadable files
        Returns list of URL of files inside the folder"""
        req = self.session.get(folder_url)
        soup = bs(req.text, "html.parser")

        folder_contents = []
        for link in soup.find_all('a', href=True):
            file_url = link.get("href")
            if "mod_folder/content" in file_url:
                folder_contents.append(file_url)
        return folder_contents

def check_config_file(config_file):
    """checks if config file exists, if not, create one"""
    configfile = HERE + config_file
    if not os.path.isfile(configfile):
        os.mknod(configfile)
        with open(configfile, 'w') as f:
            f.write('''[login]
### Put in your UC credentials here
### e.g.
### username = reddit1337
### password = hunter2
username =
password =

[courses]
### Put in your courses here
### courses has to be separated with comma(s)
### e.g.
### courses = COSC, EMTH, PHYS
courses =

[download]
### Put the download directory here
### For Linux users:
### download_directory = /home/<user>/Downloads/
###
### For macOS users:
### download_directory = /Users/<user>/Downloads/
download_directory =

[slack]
### Your Slack token here for notifications
###
### Useful if you're running this program
### inside a cronjob
token =
''')
        cprint("[INFO] Created config.txt file", "green")
        cprint("[INFO] Please edit it first before proceeding", "green")
        sys.exit(1)

