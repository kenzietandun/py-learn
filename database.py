#!/usr/bin/env python3

import os
import sqlite3 
from termcolor import cprint
from bs4 import BeautifulSoup as bs
import time
from config import HERE
from config import LOG

def init_db():
    """initialise sqlite database
    Returns sqlite connection and cursor obj
    """
    cprint("[INFO] Initialising connection to database", "green")
    dbfile = HERE + "pylearn.sqlite"
    print("Database: {}".format(dbfile))
    conn = sqlite3.connect(dbfile)
    c = conn.cursor()

    # Create table if not exists
    c.execute('''CREATE TABLE IF NOT EXISTS history
                 (url TEXT PRIMARY KEY,
                 filename TEXT)''')
    conn.commit()
    return (conn, c)

def is_downloaded(url, conn, c):
    """check if <<filename>> has been downloaded before"""
    c.execute('''SELECT * FROM history WHERE
                 url LIKE "%{}%"'''.format(url))
    res = c.fetchone()
    return res is not None

def commit(conn):
    """commit database transaction"""
    conn.commit()

def download_pdf(session, c, url, output_directory):
    """download_pdf from URL, check if the link
    contains PDF, if not, follow link and check if
    a PDF file is embedded inside"""
    ALLOWED_FILE_TYPES = [
        "application/pdf", # pdf
        "text/csv",  # csv files
        "application/zip", # zipfiles
        "application/octet-stream",  # codes
        "application/vnd.openxmlformats"
        "-officedocument.wordprocessingml.document", # docs
        "application/vnd.openxmlformats"
        "-officedocument.presentationml.presentation", # ppt
        "application/vnd.ms-powerpoint", # ppt
        "application/vnd.openxmlformats",
        "-officedocument.spreadsheetml.sheet", # excel
        "text/html; charset=iso-8859-1" # excel
        ]

    def get_url(url, output_directory):
        """download handler
        downloads { url } into { output directory }

        filename is obtained from parsing Content-Disposition
        from the headers

        if slack_token is set, add downloaded filename
        to the string of downloaded files"""

        c.execute('''INSERT OR IGNORE INTO history
                          VALUES((?), '')''', (url,))
        req = session.get(url,
                allow_redirects=True,
                stream=True,
                timeout=5)

        while req.status_code != 200:
            cprint("[ERR: {}] {}".format(req.status_code, url), "yellow")
            req = session.get(url,
                    allow_redirects=True,
                    stream=True,
                    timeout=5)
            time.sleep(1)

        if "Content-Disposition" in req.headers:
            filename = req.headers["Content-Disposition"]
            filename = filename.split('"')[-2].upper()
        else:
            filename = '-'.join(url.split('/')[-2:])
        target = "{}/{}".format(output_directory,
                filename)
        if os.path.isfile(target):
            return
        cprint("       ↓ {}".format(target),
                "green")
        with open(target, "wb") as pdf:
            for block in req.iter_content(1024):
                pdf.write(block)

    def parse_page_contents(url):
        """a handler for those times
        when a page embeds another file inside

        searches for embedded pdfs and videos

        if any of those are not found, URL of the
        page will be written to the logfile"""
        req = session.get(url)

        soup = bs(req.text, "html.parser")
        pdfs = soup.find_all("object", {"type": "application/pdf"})
        videos = soup.find_all("source", {"type", "video/mp4"})
        if pdfs:
            for pdf in pdfs:
                pdf_link = pdf.get("data")
                if pdf_link and pdf_link.endswith(".pdf"):
                    get_url(pdf_link, output_directory)
        elif videos:
            for video in videos:
                video_link = video.get("src")
                if video_link:
                    get_url(video_link, output_directory)
        else:
            with open(LOG, "a") as log:
                log.write("{}\n".format(url))

    if not os.path.isdir(output_directory):
        os.makedirs(output_directory)

    req = session.head(url, allow_redirects=True)
    file_type = req.headers["Content-Type"]
    c.execute('''INSERT OR IGNORE INTO history
                      VALUES((?), '')''', (url,))
    if file_type in ALLOWED_FILE_TYPES:
        # download file
        get_url(url, output_directory)
        time.sleep(0.2)
    else:
        # it could still be pdf,
        # it's just embedded in another page
        # SEE: http://learn.canterbury.ac.nz/mod/resource/view.php?id=574972
        parse_page_contents(url)
