## py-learn

a small script to download pdf files from LEARN website of University of Canterbury NZ

### Installation

Requirements:

- python3

- a working internet connection

- pip3

Steps:

- Clone the repository:
```
git clone https://gitlab.com/kenzietandun/py-learn
```

- Create a new virtualenv:
```
cd py-learn && python3 -m venv .
```

- Activate virtualenv:
```
source bin/activate
```

- Install requirements:
```
pip install -r req.txt
```

### Usage

To run:
```
./main.py
```

You will be asked to input your details the first time
you run it. This details are only used to login to the LEARN website.

Please report any problems to the issues tab above ^^^
